<?xml version="1.0" encoding="UTF-8"?>
<!-- SPDX-License-Identifier: GPL-3.0-or-later -->
<!-- SPDX-FileCopyrightText: 2019-2024 Gianni Lerro <glerro@pm.me> -->
<Project xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
         xmlns:foaf="http://xmlns.com/foaf/0.1/"
         xmlns:gnome="http://api.gnome.org/doap-extensions#"
         xmlns="http://usefulinc.com/ns/doap#">

    <name>gnome-shell-extension-lgbutton</name>
    <shortname>gnome-shell-extension-lgbutton</shortname>
    <shortdesc xml:lang="en">Toggle the Looking Glass visibility and more.</shortdesc>
    <description xml:lang="en">
Toggle the Looking Glass visibility by right clicking on a panel icon.
And left clicking on the icon show a menu with new features like Restart
Gnome Shell (not available on Wayland), Reload Theme, Open Extension
Folder and Open Theme Folder.
    </description>
    <homepage rdf:resource="https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton" />
    <download-page rdf:resource="https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton" />
    <bug-database rdf:resource="https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/issues" />
    <license rdf:resource="https://spdx.org/licenses/GPL-3.0-or-later.html"/>
    <category rdf:resource="http://api.gnome.org/doap-extensions#core" />
    <programming-language>JavaScript</programming-language>

    <maintainer>
        <foaf:Person>
            <foaf:name>Gianni Lerro</foaf:name>
            <foaf:mbox rdf:resource="mailto:glerro@pm.me" />
            <foaf:nick>glerro</foaf:nick>
            <gnome:userid>glerro</gnome:userid>
        </foaf:Person>
    </maintainer>
</Project>
