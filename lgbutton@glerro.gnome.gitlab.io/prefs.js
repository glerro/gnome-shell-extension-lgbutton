/* -*- Mode: js; indent-tabs-mode: nil; js-basic-offset: 4; tab-width: 4; -*- */
/*
 * This file is part of Looking Glass Button.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton
 *
 * prefs.js
 *
 * Copyright (c) 2019-2025 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Looking Glass Button is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Looking Glass Button is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Looking Glass Button. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2019-2025 Gianni Lerro <glerro@pm.me>
 */

'use strict';

import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Adw from 'gi://Adw';

import {ExtensionPreferences, gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

const TEMPLATE_ADW = 'prefs_adw1.ui';

const LGButtonPrefsWidgetAdw = GObject.registerClass({
    GTypeName: 'LGButtonPrefsWidgetAdw',
    Template: import.meta.url.replace('prefs.js', TEMPLATE_ADW),
    InternalChildren: ['localExtensionsFolder', 'systemExtensionsFolder',
        'localThemesFolder', 'systemThemesFolder'],
}, class LGButtonPrefsWidgetAdw extends Adw.PreferencesPage {
    constructor(settings, params = {}) {
        super(params);

        this._settings = settings;

        this._settings.bind('local-extensions-folder', this._localExtensionsFolder,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('system-extensions-folder', this._systemExtensionsFolder,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('local-themes-folder', this._localThemesFolder,
            'active', Gio.SettingsBindFlags.DEFAULT);

        this._settings.bind('system-themes-folder', this._systemThemesFolder,
            'active', Gio.SettingsBindFlags.DEFAULT);
    }
});

export default class LGButtonPreferences extends ExtensionPreferences {
    fillPreferencesWindow(window) {
        const page = new LGButtonPrefsWidgetAdw(this.getSettings());

        window.add(page);

        window.set_default_size(640, 450);
    }
}

