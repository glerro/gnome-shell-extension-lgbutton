/* -*- Mode: js; indent-tabs-mode: nil; js-basic-offset: 4; tab-width: 4; -*- */
/*
 * This file is part of Looking Glass Button.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton
 *
 * extension.js
 *
 * Copyright (c) 2019-2025 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Looking Glass Button is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Looking Glass Button is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Looking Glass Button. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2019-2025 Gianni Lerro <glerro@pm.me>
 */

'use strict';

import Clutter from 'gi://Clutter';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Meta from 'gi://Meta';
import St from 'gi://St';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PanelMenu from 'resource:///org/gnome/shell/ui/panelMenu.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

// Extend the Button class from Panel Menu.
let LGIndicator = GObject.registerClass({
    GTypeName: 'LGIndicator',
}, class LGIndicator extends PanelMenu.Button {
    constructor(extension) {
        super(0.0, `${extension.metadata.name} Indicator`, true);

        this._extension = extension;
        this._extensionPath = extension.path;
        this._extensionSettings = extension.getSettings();

        // Pick an icon
        this._icon = new St.Icon({
            gicon: Gio.icon_new_for_string(`${this._extensionPath}/icons/utilities-looking-glass-symbolic.svg`),
            style_class: 'system-status-icon',
        });

        // Right Click Menu
        this.rightClickMenu = new RightClickMenu(this, 0.5, St.Side.TOP);

        // Main Menu Manager
        this.menuManager = new PopupMenu.PopupMenuManager(this);
        this.menuManager._changeMenu = _menu => { };
        this.menuManager.addMenu(this.rightClickMenu);

        this.add_child(this._icon);
    }

    vfunc_event(event) {
        const type = event.type();
        let button = 0;

        if (type === Clutter.EventType.BUTTON_PRESS)
            button = event.get_button();
        else if (type === Clutter.EventType.TOUCH_BEGIN)
            button = 1;

        if (button === 1) {
            if (this.rightClickMenu.isOpen)
                this.rightClickMenu.toggle();

            if (Main.lookingGlass === null)
                Main.createLookingGlass();

            Main.lookingGlass.toggle();
            return Clutter.EVENT_STOP;
        } else if (button === 3) {
            if (Main.lookingGlass !== null && Main.lookingGlass.isOpen)
                Main.lookingGlass.toggle();

            this.rightClickMenu.toggle();
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    }
});

const RightClickMenu = class LGIndicatorRightClickMenu extends PopupMenu.PopupMenu {
    constructor(sourceActor, arrowAlignment, arrowSide) {
        super(sourceActor, arrowAlignment, arrowSide);

        this._extensionsPath = GLib.build_pathv('/', [global.userdatadir, 'extensions/']);
        this._themesPath = GLib.build_pathv('/', [GLib.get_user_data_dir(), 'themes/']);
        this._systemExtensionsPath = GLib.build_pathv('/', [global.datadir, 'extensions/']);
        this._isWayland = Meta.is_wayland_compositor();

        // Get saved settings
        this._settings = sourceActor._extensionSettings;

        Main.uiGroup.add_child(this.actor);
        this.actor.hide();

        let paramsW, paramsE, paramsT, paramsSysE, paramsSysT = { };

        // Restart Gnome Shell is not available on Wayland
        if (this._isWayland)
            paramsW = {reactive: false, can_focus: false};

        // Restart Gnome Shell
        this.restartShell = new PopupMenu.PopupMenuItem(_('Restart Shell'), paramsW);
        this.addMenuItem(this.restartShell);
        this.restartShell.connectObject('activate', () => {
            Meta.restart(_('Restarting…'), global.context);
        }, this);

        // Reload Gnome Shell Theme
        this.reloadShellTheme = new PopupMenu.PopupMenuItem(_('Reload Theme'));
        this.addMenuItem(this.reloadShellTheme);
        this.reloadShellTheme.connectObject('activate', () => {
            Main.reloadThemeResource();
            Main.loadTheme();
        }, this);

        // Separator
        this.localFoldersSeparator = new PopupMenu.PopupSeparatorMenuItem();
        this.addMenuItem(this.localFoldersSeparator);

        // Check if Extensions Folder exist
        if (!GLib.file_test(this._extensionsPath, GLib.FileTest.EXISTS))
            paramsE = {reactive: false, can_focus: false};

        // Open Gnome Shell Extensions folder
        this.extensionsFolder = new PopupMenu.PopupMenuItem(_('Open Extensions Folder'), paramsE);
        this.addMenuItem(this.extensionsFolder);
        this.extensionsFolder.connectObject('activate', () => {
            this._openPath(this._extensionsPath);
        }, this);

        // Check if Themes Folder exist
        if (!GLib.file_test(this._themesPath, GLib.FileTest.EXISTS))
            paramsT = {reactive: false, can_focus: false};

        // Open Gnome Shell Themes folder
        this.themesFolder = new PopupMenu.PopupMenuItem(_('Open Themes Folder'), paramsT);
        this.addMenuItem(this.themesFolder);
        this.themesFolder.connectObject('activate', () => {
            this._openPath(this._themesPath);
        }, this);

        // Separator
        this.systemFoldersSeparator = new PopupMenu.PopupSeparatorMenuItem();
        this.addMenuItem(this.systemFoldersSeparator);

        // Check if System Extensions Folder exist
        if (!GLib.file_test(this._systemExtensionsPath, GLib.FileTest.EXISTS))
            paramsSysE = {reactive: false, can_focus: false};

        // Open Gnome Shell System Extensions folder
        this.sysExtensionsFolder = new PopupMenu.PopupMenuItem(_('Open System Extensions Folder'), paramsSysE);
        this.addMenuItem(this.sysExtensionsFolder);
        this.sysExtensionsFolder.connectObject('activate', () => {
            this._openPath(this._systemExtensionsPath);
        }, this);

        // Search the System Themes Path
        let sysThemesPath = this._searchSystemThemesPath();

        // Check if Themes Folder exist
        if (!GLib.file_test(sysThemesPath, GLib.FileTest.EXISTS))
            paramsSysT = {reactive: false, can_focus: false};

        // Open Gnome Shell System Themes folder
        this.sysThemesFolder = new PopupMenu.PopupMenuItem(_('Open System Themes Folder'), paramsSysT);
        this.addMenuItem(this.sysThemesFolder);
        this.sysThemesFolder.connectObject('activate', () => {
            this._openPath(sysThemesPath);
        }, this);

        // Bind menu visibility to the GSettings value
        this._setMenuVisibility();
    }

    _openPath(path) {
        Gio.AppInfo.launch_default_for_uri(`file://${path}`,
            global.create_app_launch_context(0, -1));
    }

    _searchSystemThemesPath() {
        let systemThemesPath = '';
        let systemDataDirs = GLib.get_system_data_dirs();

        for (let i = 0; i < systemDataDirs.length; i++) {
            systemThemesPath =  GLib.build_pathv('/', [systemDataDirs[i], 'themes/']);
            if (GLib.file_test(systemThemesPath, GLib.FileTest.EXISTS))
                break;
        }
        return systemThemesPath;
    }

    _setMenuVisibility() {
        this._settings.bind('local-extensions-folder', this.extensionsFolder,
            'visible', Gio.SettingsBindFlags.DEFAULT
        );

        this._settings.bind('local-themes-folder', this.themesFolder,
            'visible', Gio.SettingsBindFlags.DEFAULT
        );

        this._settings.bind('system-extensions-folder', this.sysExtensionsFolder,
            'visible', Gio.SettingsBindFlags.DEFAULT
        );

        this._settings.bind('system-themes-folder', this.sysThemesFolder,
            'visible', Gio.SettingsBindFlags.DEFAULT
        );
    }
};

export default class LGButtonExtension extends Extension {
    enable() {
        console.log(`Enabling ${this.metadata.name} - Version ${this.metadata['version-name']}`);

        this._indicator = new LGIndicator(this);

        Main.panel.addToStatusArea(`${this.metadata.name} Indicator`, this._indicator);
    }

    disable() {
        console.log(`Disabling ${this.metadata.name} - Version ${this.metadata['version-name']}`);

        if (this._indicator !== null) {
            this._indicator.destroy();
            this._indicator = null;
        }
    }
}

