# Looking Glass Button
![Screenshot_01](https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/-/raw/main/assets/Screenshot_01.webp)

![Screenshot_02](https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/-/raw/main/assets/Screenshot_02.webp)

![Screenshot_03](https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/-/raw/main/assets/Screenshot_03.webp)

Toggle the Looking Glass visibility by right clicking on a panel icon.

And from version 4 left clicking on the icon show a menu with new features like 
Restart Gnome Shell (not available on Wayland), Reload Theme, Open Extension
Folder and Open Theme Folder.

Version 4 also drop the compatibility with Gnome Shell 3.30.

## Features
 * Toggle the Looking Glass visibility by right clicking on a panel icon.
 * Show a menu by left clicking on a panel icon with useful shortcuts like:
 * Restart Gnome Shell (not available on Wayland).
 * Reload Theme.
 * Open Extension Folder, user and system.
 * Open Theme Folder, user and system.
 * Preferences dialog to select which folders show in menu.
 * Support Gnome Shell Versions 3.32, 3.34, 3.36, 3.38, 40, 41, 42, 43, 44, 45, 46, 47, 48.
 * Translatable.

## Installation
Normal users are recommended to get the extension from

[![Get it on GNOME Extensions](https://raw.githubusercontent.com/andyholmes/gnome-shell-extensions-badge/master/get-it-on-ego.svg?sanitize=true){height=100px}](https://extensions.gnome.org/extension/2296/looking-glass-button/)

## Manual Installation
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

You must install git, meson, ninja and xgettext.

For a regular use and local installation these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
meson setup --prefix=$HOME/.local _build
meson install -C _build
```

Otherwise you can run the `./local_install.sh` script, it performs the build steps specified in the
previous section.

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

Finally after restarting Gnome Shell or logging in, you need to enable the extension:
```bash
gnome-extensions enable lgbutton@glerro.gnome.gitlab.io
```

## Export extension ZIP file
To create a ZIP file with the extension, just run:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
./export-zip.sh
```

This will create the file `lgbutton@glerro.gnome.gitlab.io.shell-extension.zip` with the extension, you can install it with this steps:

```bash
gnome-extensions install lgbutton@glerro.gnome.gitlab.io.shell-extension.zip
```

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>) after that, or logout and login again.

Finally after restarting Gnome Shell or logging in, you need to enable the extension:

```bash
gnome-extensions enable lgbutton@glerro.gnome.gitlab.io
```

## Translating
To contribute translations you will need a Gnome GitLab account, git, meson, ninja, xgettext and a translation program like Gtranslator.

Start by creating a fork on Gnome GitLab.

Clone your fork with git and setup a project with meson:

```bash
git clone https://gitlab.gnome.org/<your username>/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
meson setup --prefix=$HOME/.local _build
meson compile -C _build
```

Ensure the translation template is updated:

```bash
meson compile -C _build gnome-shell-extension-lgbutton-pot
```

Add the [language code](https://www.gnu.org/software/gettext/manual/html_node/Language-Codes.html) for your translation in /po/LINGUAS file.

Create the /po/\<language code\>.po file:

```bash
meson compile -C _build gnome-shell-extension-lgbutton-update-po
```

Open /po/\<language code\>.po with Gtranslator or a similar program, translate all messages and save.

To test your translation, install the extension and restart GNOME Shell:

```bash
meson setup --prefix=$HOME/.local _build --reconfigure
meson install -C _build
```

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

When you're happy with your translation, commit the changes and push them to your fork:

```bash
git add po/LINGUAS po/<language code>.po
git commit -m "New translations (<language>)"
git push
```

Finally, open a New Merge Request from your fork at https://gitlab.gnome.org/<your_username>/gnome-shell-extension-lgbutton.

## Donating 💳️
If you like my work and want to support it, consider [donating](https://ko-fi.com/glerro). 🙂️ Thanks!

## License
Looking Glass Button - Copyright (c) 2019-2025 Gianni Lerro {glerro} ~ <glerro@pm.me>

Looking Glass Button is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Looking Glass Button is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Looking Glass Button. If not, see <https://www.gnu.org/licenses/>.

